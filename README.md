# Meet And Greet - Robot

A ROS project folder to interface with the relevant Flask Server in a "meet and greet" environment. Containing Launch file and main python node

## Requirements

Ubuntu 18.04.

ROS Melodic.

Both the "Turtlebot" and "Turtlebot3" ROS packages.

## Execution

### Simulation

Modify the source file "/src/project_multi.py" to ensure "URL" (line 26) points to the IP address of the schedulling server

Note -- The schedulling server MUST be running before performing the following steps! 

Define the required robots within the "/launch/includes/robots.launch" file.

Launch the gazebo instance and Turtlebot control software with `roslaunch project gazebo_multi_TB3.launch`.

### In-Person

Modify the source file "/src/project.py" to ensure "URL" (line 26) points to the IP address of the schedulling server

Ensure the Turtlebot2 is fully connected to the control laptop and powered on. 

Launch the required nodes with: `roslaunch project project.launch map_file:=<Path to map file for operational environment>`.

Note -- The schedulling server MUST be running before performing the following step!

Launch the control node with: `rosrun project project.py`.


