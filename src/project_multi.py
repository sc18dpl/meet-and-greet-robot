#!/usr/bin/env python

import rospy
import sys
import os

import threading

import requests
import time

from datetime import datetime

import math

from nav_msgs.msg import OccupancyGrid
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import PoseWithCovarianceStamped
from kobuki_msgs.msg import ButtonEvent, BumperEvent
from sensor_msgs.msg import LaserScan
from ar_track_alvar_msgs.msg import AlvarMarkers

import actionlib


URL = "http://192.168.68.119:5000" # Set this to the base IP of the website - for example: 127.0.0.1:5000
StateURL = str(URL) + "/API/State"
ClientURL = str(URL) + "/API/Client"
PingURL = str(URL) + "/API/Ping"
RegisterURL = str(URL) + "/API/Register"
WaypointURL = str(URL) + "/API/Waypoint"
WaitingWaypointURL = str(URL) + "/API/WaitingWaypoint"

Button = None # What button has been pressed on the bot

Verbose = True # Should we should debug prints. Passed at initialisation

RobotID = None # ID returned from the server at registration
Name = None # Name of the bot, passed at initialisation
Model = None # Model of the bot. Not super necessary, but nice for the monitor view

CurrentWaitingWaypoint = None

Resolution = None # Resolution of the map
Map = [] # List containing the map data

MarkerIDs = [] # List of QR Marker ID's that the bot can currently see
MarkerPOSs = [] # List of relative positions of QR markers from MarkerIDs

FrontDistance = 0

XCoordinate = 0 # Bots XCoordinate - Read from ODOM and reported to server via ping
YCoordinate = 0 # Bots YCoordinate

Run = True # Are we currently "running" (Should we send pings)

Client = None # stores the Client reference for move_base

Simulate = True # Should we treat this as a simulated run?


def attemptLocalise(state): # localise your location. Call this when the bot spins up
    global Simulate, CurrentWaitingWaypoint

    if (Simulate == True): # Were gonna skip this if were simulating
        return True
    
    failureCount = 0
    result = False

    failureCountMax = 3 # This ideally needs to be low to avoid bottlenecks in the system

    response = requests.get(WaypointURL, params = {
        "name" : CurrentWaitingWaypoint
    })

    waypointID = None

    if response.status_code == 200:
        jsonData = response.json()
        waypointID = jsonData["id"]

    while (result == False and failureCount < failureCountMax):
        if (waypointID in MarkerIDs): # MarkerIDs is automatically updated by the callback for any markers we can currently see
            result = True
        
        else:
            time.sleep(1) # Wait a second to see if the marker shows up
            failureCount += 1 # Try again after waiting

    return result

def goalPoseCreator(x, y, z=0, w=0):
    global Verbose

    goalPose = MoveBaseGoal()

    goalPose.target_pose.header.frame_id = "map"
    goalPose.target_pose.pose.position.x = float(x)
    goalPose.target_pose.pose.position.y = float(y)
    goalPose.target_pose.pose.position.z = 0.0

    goalPose.target_pose.pose.orientation.x = 0.0
    goalPose.target_pose.pose.orientation.y = 0.0
    goalPose.target_pose.pose.orientation.z = z
    goalPose.target_pose.pose.orientation.w = w
    
    return goalPose

def move(goalPose):    
    global Simulate, XCoordinate, YCoordinate, Client, Verbose

    if Verbose:
        print "Driving"

    Client.cancel_goal()
    Client.send_goal(goalPose)
    Client.wait_for_result()


def home(stateData):

    # 
    # Make a request to the server to get our closest waiting position
    # Travel to that waiting position, set state to "available"
    #

    global RobotID, Client, XCoordinate, YCoordinate

    response = requests.get(WaitingWaypointURL, params = {
        "robotID" : RobotID,
        "xCoord" : XCoordinate,
        "yCoord" : YCoordinate
    })

    if response.status_code == 200: # Valid response from the server
        jsonData = response.json()

        xCoord = jsonData["xCoord"]
        yCoord = jsonData["yCoord"]
        directionZ = float(jsonData["directionZ"])
        directionW = float(jsonData["directionW"])

        goalPose = goalPoseCreator(xCoord, yCoord, directionZ, directionW)
        move(goalPose)

        # We have arrived at our waiting point, so lets tell the server we are ready for a request
        response = requests.put(StateURL, data = {
            "robotID" : RobotID,
            "state" : "available"
        })

def client(stateData):

    #
    # Travel to the clients location
    # 

    global RobotID

    # Before anything, perform a locatisation check!
    if (attemptLocalise("client") == False):
        # Were lost. This is bad, need to let the server know so it can schedule another bot ASAP!

        response = requests.put(StateURL, data = {
            "robotID" : RobotID,
            "state" : "lost - Client", # "- Client" to tell the server to schedule another bot
            "lastWaypoint" : CurrentWaitingWaypoint
        })

        return # Lets not do anything else

    # First we need to travel to the clients location
    
    response = requests.get(StateURL, params = {
        "robotID" : RobotID
    })

    if response.status_code == 200:
        jsonData = response.json()

        xCoord = jsonData["xCoord"]
        yCoord = jsonData["yCoord"]
        directionZ = float(jsonData["directionZ"])
        directionW = float(jsonData["directionW"])

        goalPose = goalPoseCreator(xCoord, yCoord, directionZ, directionW)

        if Verbose == "True":
            print "moving to clients location"

        move(goalPose)

        if Verbose == "True":
            print "arrived at clients location"

        # We have arrived at the client, so lets tell the server were waiting for the button press
        # However, were gonna use ClientRequest.put to handle this due to custom logic
        response = requests.put(ClientURL, data = {
            "robotID" : RobotID,
            "client" : True
        })

    else:
        if Verbose == "True":
            print "Waypoint does not exist!! Passing to try again"

        return

def waiting(stateData):

    global Verbose, Button, RobotID, Name, Simulate

    #
    # Wait for the user to press the button
    # Ping the server regularly to see if the bot has timed-out
    #

    # Were at the clients location, we need to wait for them to press B0

    state = "waiting"

    lastRequest = datetime.now()

    Button = None # Clear any previous button presses

    while (state == "waiting" and Button != 0):
        time = datetime.now()
        delta = time - lastRequest

        if (delta.seconds >= 5):
            if Simulate:
                Button = 0

            lastRequest = datetime.now()

            response = requests.get(StateURL, params = {
                "robotID" : RobotID
            })

            if response.status_code != 400:
                jsonData = response.json()

                state = jsonData["state"]

    if Button == 0: # we have broken out due to a valid button press
        Button = None # Clear this button presses as we have recognised it

        # We should tell the server we have found our client and are travelling to the destination

        if Verbose == "True":
            print "Client found us. Travelling to destination"

        response = requests.put(StateURL, data = {
            "robotID" : RobotID,
            "state" : "travelling"
        })

    elif state == "available": # we have broken out due to a waiting timeout
        if Verbose == "True":
            print "Request has timed out - Returned to available state"

def travelling(stateData, attempts = 0):

    global Verbose, RobotID, Client

    #
    # Travel to the target location
    # Set state to home and return
    #

    # First we need to get the target coordinates
    response = requests.get(StateURL, params = {
        "robotID" : RobotID
    })

    if response.status_code == 200:
        jsonData = response.json()

        xCoordinate = jsonData["xCoord"]
        yCoordinate = jsonData["yCoord"]
        directionZ = float(jsonData["directionZ"])
        directionW = float(jsonData["directionW"])


        goalPose = goalPoseCreator(xCoordinate, yCoordinate, directionZ, directionW)
        move(goalPose)

        # We should have arrived at the target location
        # So lets play a little song or something

        if Verbose:
            print "Arrived!! Waiting for 5 seconds before moving on"

        time.sleep(5)

        response = requests.put(StateURL, data = {
            "robotID" : RobotID,
            "state" : "home"
        })

        lastRequest = datetime.now()

        while response.status_code != 200:
            if (datetime.now - lastRequest).seconds > 5:
                if Verbose == "True":
                    print "Invalid transition to 'home', trying again"

                response = requests.put(StateURL, data = {
                    "robotID" : RobotID,
                    "state" : "home"
                })

                lastRequest = datetime.now()

        response = requests.put(ClientURL, data = { # Make another call to the Client endpoint to see if there is another waypoint or not
            "robotID" : RobotID,
            "client" : False
        })

    elif response.status_code == 201: # valid response, but invalid waypoint data
        if Verbose == "True":
            print "Invalid waypoint coordinates!"

    else:
        if attempts % 5 == 0:
            if Verbose == "True":
                print "Tried: " + attempts + " times to get waypoint data. Please help!!"

        time.sleep(5)
        travelling(jsonData, attempts + 1) # try again? 

def available(stateData):

    global Verbose, RobotID, CurrentWaitingWaypoint

    # While were waiting for the client, we can check the localisation QR codes
    # If we can't find our code, we might be lost, and should report
    # that back to the server!!

    if (stateData["waypointName"][:2] != "__"):
        CurrentWaitingWaypoint = stateData["waypointName"] # Update for use in the QR localisation

    if (attemptLocalise("available") == False):
        response = requests.put(StateURL, data = {
            "robotID" : RobotID,
            "state" : "lost"
        })

        if Verbose == "True":
            print "Can't find QR Code!!! Could be lost, reported to server!!"
            print "No longer available for client requests"

    else: # We have successfully localised with the QR codes, business as normal
        if Verbose == "True":
            print "Waiting for clients"

def lost(stateData):
    # This should keep performing localisation until it finds the QR code
    # Then transition back to "available"

    global RobotID

    print (MarkerIDs)

    if (attemptLocalise("lost")):
        # Weve localised, someone must have put us back. lets tell the server

        response = requests.put(StateURL, data = {
            "robotID" : RobotID,
            "state" : "available"
        })

    # Else, stay as lost and keep looping this until someone sorts us out


def buttonCallback(data):
    global Button, Verbose

    if (data.state != ButtonEvent.RELEASED): # We don't care about release events
        if (data.button == ButtonEvent.Button0):
            Button = 0
        elif (data.button == ButtonEvent.Button1):
            Button = 1
        elif (date.button == ButtonEvent.Button2):
            Button = 2

        if Verbose == "True":
            print "Button " + str(Button) + " pressed"

def bumperCallback(data):
    global Verbose

    if (data.state != ButtonEvent.RELEASED):
        if Verbose == "True":
            print "Ow. I crashed"

def amclCallback(data):
    global XCoordinate, YCoordinate

    XCoordinate = data.pose.pose.position.x
    YCoordinate = data.pose.pose.position.y

def mapCallback(data):
    width = data.info.width
    height = data.info.height

    global Resolution, Map
    Resolution = float(str(data.info.resolution)[:6])

    mapData = data.data
    
    Map = []
    Map.append([mapData[0]]) # append the first row

    rowCount = 0

    for i in range(1, len(mapData)): # Step over the whole 1D map
        if (i % width == 0 and i != 0): # Weve got to the end of the "row"
            Map.append([])
            rowCount = rowCount + 1 # Create a new row, and index it

        Map[rowCount].append(mapData[i])

def scanCallback(data):
    global FrontDistance

    FrontDistance = data.ranges[320] # Get the distance to the object infront of the bot

def QRCallback(data):

    global MarkerIDs, MarkerPOSs

    markerID = [] #initialise lists for marker IDs and Pose
    markerPose = []
    #loop of markers found
    #if they have a valid id store them
    for m in range(len(data.markers)):
        mID = data.markers[m].id

        markerID.append(mID)
        markerPose.append(data.markers[m].pose.pose.position)

    MarkerIDs = markerID
    MarkerPOSs = markerPose


def initialiseCallbacks():
    global Simulate, Client, Name

    Client = actionlib.SimpleActionClient("/" + str(Name) + "/move_base", MoveBaseAction)
    Client.wait_for_server()

    try:
        rospy.Subscriber("/" + str(Name) + "/amcl_pose", PoseWithCovarianceStamped, amclCallback)
    except:
        print "Unable to subscribe to amcl!!"
        exit(1)

    try:
        rospy.Subscriber("/" + str(Name) + "/map", OccupancyGrid, mapCallback)
    except:
        print "Unable to subscribe to map!!"
        exit(1)

    try:
        rospy.Subscriber("/" + str(Name) + "/scan", LaserScan, scanCallback)
    except:
        print "Unable to subscribe to scan!!"
        exit(1)

    if (Simulate == False):
        try:
            rospy.Subscriber("/mobile_base/events/button", ButtonEvent, buttonCallback)
        except:
            print "Unable to subscribe to button presses!!"
            exit(1)
        
        try:
            rospy.Subscriber("/mobile_base/events/bumper", BumperEvent, bumperCallback)
        except:
            print "Unable to subscribe to bumper hits!!"
            exit(1)

        try:
            rospy.Subscriber("/ar_pose_marker", AlvarMarkers, QRCallback) # Marker not Markers on the T_Bot laptops
        except:
            print "Unable to subscribe to QR codes!!"
            exit(1)


def ping():
    global Run

    while not rospy.is_shutdown():
        time.sleep(10)

        try:
            requests.put(PingURL, data = {
                "robotID" : RobotID,
                "xCoordinate" : XCoordinate,
                "yCoordinate" : YCoordinate
            })

        except requests.exceptions.RequestException as e:
            pass

def register():

    global Verbose, Name, Model, RobotID

    response = requests.put(RegisterURL, data = {
        "name" : str(Name),
        "model" : int(Model)
    })

    if (response.status_code == 200 or response.status_code == 201):
        # Bot has been successfully registered with the server
        RobotID = int(response.text)

    else:
        if Verbose == "True":
            print "Unable to register bot with server!!"
            print response.text

        exit(1)


def mainLogic():

    global Verbose, RobotID, Run, Name

    while not rospy.is_shutdown():

        # Check were still registered with the server
        try:
            response = requests.get(RegisterURL, params = {
                "robotID" : RobotID,
                "name" : Name
            })

            if (response.status_code == 200): # Bot has been removed from the server!! Need to re-register
                register()

                if Verbose == "True":
                    print "Bot was removed from server! Re-registered"

                continue

        except requests.exceptions.RequestException as e:
            pass

        try:
            response = requests.get(StateURL, params = { # Fetch a state from the server
                "robotID" : int(RobotID)
            })

            if (response.status_code != 200 and response.status_code != 201):
                if Verbose == "True":
                    print "state error: " + response.text

            else: # valid response
                jsonData = response.json()
                state = jsonData["state"]
                
                if state == "home":
                    if Verbose == "True":
                        print "home"
                    
                    home(jsonData)

                elif state == "client":
                    if Verbose == "True":
                        print "client"

                    client(jsonData)

                elif state == "waiting":
                    if Verbose == "True":
                        print "waiting"

                    waiting(jsonData)

                elif state == "travelling":
                    if Verbose == "True":
                        print "travelling"

                    travelling(jsonData)

                elif state == "available":
                    if Verbose == "True":
                        print "available"

                    available(jsonData)
                
                elif state == "lost":
                    if Verbose == "True":
                        print "lost!!!! ----------------------------------" # Make sure this is visible to someone checking the bot

                    lost(jsonData)

        except requests.exceptions.RequestException as e:
            pass

        time.sleep(5)

    Run = False

def main():
    if (len(sys.argv) < 3):
        print "usage: WaypointTest.py <Name> <Model> <Verbose> <X-Coord> <Y-Coord>"
        exit(1)

    else:
        global Verbose, Name, Model, Simulate, XCoordinate, YCoordinate

        Name = str(sys.argv[1])
        Model = str(sys.argv[2])
        Verbose = str(sys.argv[3])
        XCoordinate = float(sys.argv[4])
        YCoordinate = float(sys.argv[5])

        register() # Add this bot to the server
        threading.Timer(1, ping).start() # Start the ping loop

        rospy.init_node("project_" + str(Name))

        initialiseCallbacks() # Ensure this is uncommented for actual runs

        mainLogic()


if __name__ == "__main__":
    main()
